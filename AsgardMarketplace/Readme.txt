﻿Requirements:
VS 2017
node.js
.net core 2.0.9 sdk

Open NuGetPackage Manager and check if all required dependencies are installed. if not install them.(Hangfire,Nlog,Swashbuckle...)
Then just run the project, the swagger API page should load and you should see API's.
Also you can take a look at the hangfire job dashboard, to see the automatic job that rus every minute(job's should appear after 1 min)
localhost:YYYYY/hangfire

some other notes about the solution:
used dependency injection
there is an integration test project which tests logic from creation of buyers&sellers to payment&item transfer.
used nlog for logging
Notification service logs notifications(transfered payments/items) to c:/temp/notifications
Separate validation logic in the validators
Used auto-mapper to map and return VieModels in User & OrderPayment controllers, basically smaller Model with less fields
In those same controllers I'm returning a DataViewModel object, which has the response state and value.
Used ef core in memory db, does not support transactions, but this idea of unitOfWork should be visible(roll-backing all changes in the transaction)
