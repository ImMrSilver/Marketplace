﻿using System;
using System.Collections.Generic;
using AsgardMarketplace.DataViewModels;
using AsgardMarketplace.Models;
using AsgardMarketplace.Services;
using AsgardMarketplace.Validators;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace AsgardMarketplace.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly UserValidator _validator;

        public UserController(IUserService userService, UserValidator validator)
        {
            _userService = userService;
            _validator = validator;
        }

        [HttpGet]
        public DataViewModel<IList<UserViewModel>> Get()
        {
            var users = _userService.Get();
            var usersView = Mapper.Map< IList<User>, IList< UserViewModel> >(users);
            var dataViewModel = new DataViewModel<IList<UserViewModel>>(usersView);
            return dataViewModel;
        }

        [HttpGet("{id}")]
        public DataViewModel<UserViewModel> Get(string id)
        {
            var exists = _validator.UserExists(Guid.Parse(id));
            if (exists != null)
            {
                return new DataViewModel<UserViewModel>(null,exists);
            }
            var users = _userService.Get(Guid.Parse(id));
            var usersView = Mapper.Map<User, UserViewModel>(users);
            var dataViewModel = new DataViewModel<UserViewModel>(usersView);
            return dataViewModel;
        }

        [HttpPost]
        public DataViewModel<UserViewModel> Post(UserViewModel user)
        {
            var userModel = Mapper.Map<UserViewModel, User>(user);
            var error = _validator.Validate(userModel);
            if (error != null) {
                return new DataViewModel<UserViewModel>(null, error);
            }
            var createdUser = _userService.Post(userModel);
            var userView = Mapper.Map<User,UserViewModel>(createdUser);
            var dataViewModel = new DataViewModel<UserViewModel>(userView);
            return dataViewModel;
        }

        [HttpPut]
        public DataViewModel<User> Put(UserViewModel user)
        {
            var userModel = Mapper.Map<UserViewModel, User>(user);
            var exists = _validator.UserExists(user.Id);
            var error = _validator.Validate(userModel);
            if (error != null || exists != null)
            {
                return new DataViewModel<User>(null, error ?? exists);
            }
            var updatedUser = _userService.Put(userModel);
            var dataViewModel = new DataViewModel<User>(updatedUser);
            return dataViewModel;
        }

        [HttpDelete]
        public bool Delete(string id)
        {
            var IsDeleted = _userService.Delete(Guid.Parse(id));
            return IsDeleted;
        }

    }
}
