﻿using System;
using System.Collections.Generic;
using AsgardMarketplace.Models;
using AsgardMarketplace.Services;
using Microsoft.AspNetCore.Mvc;

namespace AsgardMarketplace.Controllers
{
    [Route("api/[controller]")]
    public class OrderController : Controller
    {
        private readonly IOrderService _orderService;
        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpGet("{id}")]
        public Order Get(string id)
        {
            var order = _orderService.Get(Guid.Parse(id));
            return order;
        }

        [HttpGet]
        public IList<Order> Get()
        {
            var orders = _orderService.Get();
            return orders;
        }

        [HttpPost]
        public Order Post(Order order)
        {
            var createdOrder = _orderService.Post(order);
            return createdOrder;

        }

        [HttpPut]
        public Order Put(Order order)
        {
            var updatedOrder = _orderService.Put(order);
            return updatedOrder;
        }

        // DELETE api/<controller>/5
        [HttpDelete]
        public bool Delete(string id)
        {
           var IsDeleted = _orderService.Delete(Guid.Parse(id));
           return IsDeleted;
        }
    }
}
