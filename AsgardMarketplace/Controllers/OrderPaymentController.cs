﻿using System;
using System.Collections.Generic;
using AsgardMarketplace.DataViewModels;
using AsgardMarketplace.Models;
using AsgardMarketplace.Validators;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace AsgardMarketplace.Controllers
{
    [Route("api/[controller]")]
    public class OrderPaymentController : Controller
    {
        private readonly IOrderPaymentService _orderPaymentService;
        private readonly OrderPaymentValidator _validator;

        public OrderPaymentController(IOrderPaymentService orderPaymentService, OrderPaymentValidator validator)
        {
            _orderPaymentService = orderPaymentService;
            _validator = validator;
        }

        [HttpGet]
        public DataViewModel<IList<OrderPaymentViewModel>> Get()
        {
            var OrderPayments = _orderPaymentService.Get();
            var orderPaymentViewModel = Mapper.Map<IList<OrderPayment>,IList <OrderPaymentViewModel> >(OrderPayments);
            var dataviewModel = new DataViewModel<IList<OrderPaymentViewModel>>(orderPaymentViewModel);
            return dataviewModel;
        }

        [HttpGet("{id}")]
        public DataViewModel<OrderPaymentViewModel> Get(string id)
        {
            var exists = _validator.UserExists(Guid.Parse(id));
            if (exists != null)
            {
                return new DataViewModel<OrderPaymentViewModel>(null, exists);
            }

            var OrderPayments = _orderPaymentService.Get(Guid.Parse(id));
            var orderPaymentViewModel = Mapper.Map<OrderPayment, OrderPaymentViewModel>(OrderPayments);
            var dataviewModel = new DataViewModel<OrderPaymentViewModel>(orderPaymentViewModel);
            return dataviewModel;
        }

        [HttpPost]
        public DataViewModel<OrderPaymentViewModel> Post(OrderPaymentViewModel orderPayment)
        {
            var orderPaymentModel = Mapper.Map<OrderPaymentViewModel,OrderPayment> (orderPayment);
            var errors = _validator.Validate(orderPaymentModel);
            if (errors != null)
            {
                return new DataViewModel<OrderPaymentViewModel>(null, errors);
            }
            var createdOrderPayment = _orderPaymentService.ProcessOrderPayment(orderPaymentModel);
            var orderPaymentViewModel = Mapper.Map<OrderPayment, OrderPaymentViewModel>(createdOrderPayment);
            var dataviewModel = new DataViewModel<OrderPaymentViewModel>(orderPaymentViewModel);
            return dataviewModel;
        }
    }
}
