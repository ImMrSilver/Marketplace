﻿using System;
using System.Collections.Generic;
using AsgardMarketplace.Models;
using AsgardMarketplace.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AsgardMarketplace.Controllers
{
    [Route("api/[controller]")]
    public class ItemController : Controller
    {

        private readonly IItemService _itemService;
        public ItemController(IItemService itemService)
        {
            _itemService = itemService;
        }

        [HttpGet]
        public IList<Item> Get()
        {
            var items = _itemService.Get();
            return items;
        }

        [HttpGet("{id}")]
        public Item Get(string id)
        {
            var item = _itemService.Get(Guid.Parse(id));
            return item;
        }

        [HttpPost]
        public Item Post(Item item)
        {
            var createdItem = _itemService.Post(item);
            return createdItem;
        }

        [HttpPut]
        public Item Put(Item item)
        {
            var updatedOrder = _itemService.Put(item);
            return updatedOrder;
        }

        [HttpDelete]
        public bool Delete(string id)
        {
            var IsDeleted = _itemService.Delete(Guid.Parse(id));
            return IsDeleted;
        }
    }
}
