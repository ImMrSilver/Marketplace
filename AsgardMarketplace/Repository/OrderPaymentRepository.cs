﻿using AsgardMarketplace.Models;
using AsgardMarketplace.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AsgardMarketplace
{
    public class OrderPaymentRepository : IOrderPaymentRepository
    {
        private readonly AsgardMarketplaceContext _context;

        public OrderPaymentRepository(AsgardMarketplaceContext context)
        {
            _context = context;
        }

        public OrderPayment Get(Guid id)
        {
            var orderPayment = _context.OrderPayments.Where(x => x.Id == id).FirstOrDefault();
            return orderPayment;
        }

        public IList<OrderPayment> Get()
        {
            var orderPayments = _context.OrderPayments.OrderByDescending(a=>a.CreationDate).ToList();
            return orderPayments;
        }

        public OrderPayment CreateOrderPaymentAndUpdateOrderStatus(OrderPayment orderPayment, Order order)
        {
            using (var currentOperationcontext  = _context.ShallowCopy())
            {
                using (var transaction = currentOperationcontext.Database.BeginTransaction())
                {
                    try
                    {
                        var createdOrderPayment = _context.OrderPayments.Add(orderPayment);
                        currentOperationcontext.SaveChanges();
                        currentOperationcontext.Orders.Update(order);
                        currentOperationcontext.SaveChanges();

                        // Commit transaction if all commands succeed, transaction will auto-rollback
                        // when disposed if either commands fails
                        transaction.Commit();

                        return createdOrderPayment.Entity;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("CreateOrderPaymentAndUpdateOrderStatus failed with msg: " + ex.Message);
                    }
                }
            }
        }

        public OrderPayment Put(OrderPayment orderPayment)
        {
            var currentOrderPayment = Get(orderPayment.Id);
            currentOrderPayment.Taxes = orderPayment.Taxes;

            currentOrderPayment.TotalOrderSum = orderPayment.TotalOrderSum;
            currentOrderPayment.TotalOrderSumAfterTax = orderPayment.TotalOrderSumAfterTax;
            currentOrderPayment.IsCompleted = orderPayment.IsCompleted;

            var updatedUser = _context.OrderPayments.Update(currentOrderPayment);
            _context.SaveChanges();
            return updatedUser.Entity;
        }
    }
}