﻿using AsgardMarketplace.Models;
using AsgardMarketplace.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AsgardMarketplace
{
    public class ItemRepository : IItemRepository
    {
        private readonly AsgardMarketplaceContext _context;

        public ItemRepository(AsgardMarketplaceContext context)
        {
            _context = context;
        }

        public Item Get(Guid ItemId)
        {
            var Item = _context.Items.Where(x => x.Id == ItemId).FirstOrDefault();
            return Item;
        }

        public IList<Item> Get()
        {
            var Items = _context.Items.ToList();
            return Items;
        }

        public Item Post(Item Item)
        {
          var createdItem = _context.Items.Add(Item);
          _context.SaveChanges();
          return createdItem.Entity;
        }

        public Item Put(Item Item)
        {
            var user = Get(Item.Id);

            user.Name = Item.Name;
            user.Description = Item.Description;
            user.ItemAsByteStream = Item.ItemAsByteStream;
            user.Price = Item.Price;
            user.Status = Item.Status;
            user.TaxRule = Item.TaxRule;

            var updatedItem = _context.Items.Update(user);
            _context.SaveChanges();
            return updatedItem.Entity;
        }

        public bool Delete(Guid userId)
        {
            try
            {
                var Item = Get(userId);
                _context.Items.Remove(Item);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}