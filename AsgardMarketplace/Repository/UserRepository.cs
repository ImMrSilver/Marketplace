﻿using AsgardMarketplace.Models;
using AsgardMarketplace.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AsgardMarketplace
{
    public class UserRepository : IUserRepository
    {
        private readonly AsgardMarketplaceContext _context;

        public UserRepository(AsgardMarketplaceContext context)
        {
            _context = context;
        }

        public User Get(Guid UserId)
        {
            var User = _context.Users.Where(x => x.Id == UserId).FirstOrDefault();
            return User;
        }

        public IList<User> Get()
        {
            var Users = _context.Users.ToList();
            return Users;
        }

        public User Post(User User)
        {
          var createdUser = _context.Users.Add(User);
          _context.SaveChanges();
          return createdUser.Entity;
        }

        public User Put(User user)
        {
            var currentUser = _context.Users.Where(x => x.Id == user.Id).FirstOrDefault();
            currentUser.Name = user.Name;
            currentUser.SureName = user.SureName;
            currentUser.Email = user.Email;
            currentUser.AccountBalance = user.AccountBalance;
            currentUser.CreditCardNumber = user.CreditCardNumber;
            currentUser.About = user.About;
            var updatedUser = _context.Users.Update(currentUser);
            
            _context.SaveChanges();
            return updatedUser.Entity;
        }

        public bool Delete(Guid userId)
        {
            try
            {
                var User = Get(userId);
                _context.Users.Remove(User);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}