﻿using AsgardMarketplace.Models;
using AsgardMarketplace.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AsgardMarketplace
{
    public class OrderRepository : IOrderRepository
    {
        private readonly AsgardMarketplaceContext _context;

        public OrderRepository(AsgardMarketplaceContext context)
        {
            _context = context;
        }

        public bool Delete(Guid orderId)
        {
            try
            {
                var order = Get(orderId);
                _context.Orders.Remove(order);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void DeleteUnpaidOrders()
        {
            var unpaidOrders = _context.Orders.Where(x =>
                (DateTime.Now - x.CreationDate).TotalHours >= 2 &&
                 x.Status == OrderStatus.Created).ToList();
            _context.Orders.RemoveRange(unpaidOrders);
            _context.SaveChanges();
        }

        public Order Get(Guid orderId)
        {
            var order = _context.Orders.Where(x => x.Id == orderId).FirstOrDefault();
            return order;
        }

        public IList<Order> Get()
        {
            var orders = _context.Orders.OrderByDescending(a=>a.CreationDate).ToList();
            return orders;
        }

        public Order Post(Order order)
        {
            var createdOrder = _context.Orders.Add(order);
            _context.SaveChanges();
            return createdOrder.Entity;
        }

        public Order Put(Order order)
        {
            var currentOrder = Get(order.Id);
            currentOrder.Price = order.Price;
            currentOrder.Status = order.Status;
            currentOrder.SellerId = order.SellerId;
            currentOrder.BuyerId = order.BuyerId;
            currentOrder.ItemId = order.ItemId;

            var updatedUser = _context.Orders.Update(currentOrder);
            _context.SaveChanges();
            return updatedUser.Entity;
        }
    }
}