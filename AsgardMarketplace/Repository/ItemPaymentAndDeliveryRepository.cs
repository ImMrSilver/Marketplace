﻿using AsgardMarketplace.Models;
using AsgardMarketplace.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AsgardMarketplace
{
    public class ItemPaymentAndDeliveryRepository : IItemPaymentAndDeliveryRepository
    {
        private readonly AsgardMarketplaceContext _context;

        public ItemPaymentAndDeliveryRepository(AsgardMarketplaceContext context)
        {
            _context = context;
        }

        public (User,User,OrderPayment,Item) ProcessItemPaymentAndDelivery(User seller, User buyer, OrderPayment orderPayment, Item item)
        {
            using (var currentOperationcontext = _context.ShallowCopy())
            {
                using (var transaction = currentOperationcontext.Database.BeginTransaction())
                {
                    try
                    {
                        var updatedSeller = currentOperationcontext.Users.Update(seller);
                        var updatedBuyer = currentOperationcontext.Users.Update(buyer);
                        var updatedOrderPayment = currentOperationcontext.OrderPayments.Update(orderPayment);
                        var updatedItem = currentOperationcontext.Items.Update(item);

                        currentOperationcontext.SaveChanges();

                        // Commit transaction if all commands succeed, transaction will auto-rollback
                        // when disposed if either commands fails
                        transaction.Commit();

                        return (updatedSeller.Entity, updatedBuyer.Entity, updatedOrderPayment.Entity, updatedItem.Entity);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("CreateOrderPaymentAndUpdateOrderStatus failed with msg: " + ex.Message);
                    }
                }
            }
        }

    }
}