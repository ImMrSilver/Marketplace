﻿using AsgardMarketplace.Models;
using Microsoft.EntityFrameworkCore;

namespace AsgardMarketplace.Repository
{
    public class AsgardMarketplaceContext : DbContext
    {
        public AsgardMarketplaceContext(DbContextOptions<AsgardMarketplaceContext> options)
          : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderPayment> OrderPayments { get; set; }

        public AsgardMarketplaceContext ShallowCopy()
        {
            return (AsgardMarketplaceContext) MemberwiseClone();
        }
    }
}
