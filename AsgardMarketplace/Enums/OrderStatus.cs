﻿namespace AsgardMarketplace.Models
{
    public enum OrderStatus
    {
        Created,
        RegisterForPayment,
        Closed
    }
}
