﻿namespace AsgardMarketplace.Models
{
    public enum TaxRule
    {
        // Tax for item charged by the marketplace, depending on the item
        Percent_5, 
        Percent_10,
        Percent_25,
    }
}
