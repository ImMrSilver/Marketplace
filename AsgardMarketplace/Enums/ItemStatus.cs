﻿namespace AsgardMarketplace.Models
{
    public enum ItemStatus
    {
        ForSale,// Show in marketplace
        OrderCreated, // Do not show in marketplace
        Inventory// Do not shown in marketplace, only user can see it
    }
}
