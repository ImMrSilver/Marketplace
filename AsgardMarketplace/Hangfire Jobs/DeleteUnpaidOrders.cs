﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AsgardMarketplace.Models;

namespace AsgardMarketplace.Services
{
    public class DeleteUnpaidOrders
    {
        private IOrderRepository _orderRepository;

        public DeleteUnpaidOrders(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public void Delete()
        {
           _orderRepository.DeleteUnpaidOrders();
        }
        
    }
}
