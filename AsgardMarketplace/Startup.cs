using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AsgardMarketplace.DataViewModels;
using AsgardMarketplace.Models;
using AsgardMarketplace.Repository;
using AsgardMarketplace.Services;
using AsgardMarketplace.Validators;
using AutoMapper;
using Hangfire;
using Hangfire.MemoryStorage;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace AsgardMarketplace
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserRepository, UserRepository>();

            services.AddScoped<IOrderService,OrderService>();
            services.AddScoped<IOrderRepository, OrderRepository>();

            services.AddScoped<IItemService, ItemService>();
            services.AddScoped<IItemRepository, ItemRepository>();

            services.AddScoped<IOrderPaymentService, OrderPaymentService>();
            services.AddScoped<IOrderPaymentRepository, OrderPaymentRepository>();

            services.AddScoped<INotificationService, NotificationService>();

            services.AddScoped<IItemPaymentAndDeliveryService, ItemPaymentAndDeliveryService>();
            services.AddScoped<IItemPaymentAndDeliveryRepository, ItemPaymentAndDeliveryRepository>();

            services.AddScoped<BaseValidator>();
            services.AddScoped<UserValidator>();
            services.AddScoped<OrderPaymentValidator>();

            services.AddDbContext<AsgardMarketplaceContext>(opt =>
                { opt.UseInMemoryDatabase("AsgardMarketplace");
                  opt.ConfigureWarnings(x => x.Ignore(Microsoft.EntityFrameworkCore.Diagnostics.InMemoryEventId.TransactionIgnoredWarning));
                });
            var inMemory = GlobalConfiguration.Configuration.UseMemoryStorage();
            services.AddHangfire(x => x.UseStorage(inMemory));
            services.AddMvc();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "AsgardMarketplace API",
                    Description = "USER API - 1. create users: sellers & buyers\n" +
                    "   POST - For buyer you should set Account Balance f.e. 50 \n" +
                    "   So that he would be able to buy an item, also save buyer id from response. You will need that for order & orderpayment API's\n" +
                    "   Save the Id of the seller too, you will need it to create an item for sale.\n"+
                    "   PUT - to update a seller/buyer, make sure to send existing user Id with your request!\n" +
                    "\n"+
                    "ITEM API - 2. create an item, for sale or just to keep in you inventory \n" +
                    "   POST - If the item is for sale set a price, and the OwnerId(sellerId)\n" +
                    "   PUT - to update an item, make sure to send existing item Id with your request!\n" +
                    "\n" +

                    "ORDER API - 3. create an order, you will have 2h to make a payment for it before it will be deleted by a hangfire job\n"
                    + "  POST - set BuyerId, SellerId, Price, ItemId" +
                    "   PUT - to update the Order make sure to send correct Id's(don't mix them up)\n" +
                    "\n"+

                    "ORDERPAYMENT API- 4. execute payment delivery from buyer to seller. Calls item delivery service that delivers the item and closes the payment.\n." +
                    
                    "POST- set buyerId, sellerId, orderId and the payment will be executed. These Ids are from other API responses\n"+

                    "Validation logic is implemented for User & OrderPayment APi's\n" +
                    "Checking if Id's exist in db & if the buyer has enough money to buy an item."

                });
            });

            HangfireJobs(services);
            Mapping();
        }

        private void Mapping()
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<UserViewModel, User>();
                cfg.CreateMap<User, UserViewModel>();

                cfg.CreateMap<OrderPaymentViewModel, OrderPayment>();
                cfg.CreateMap<OrderPayment, OrderPaymentViewModel>();
            });
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions { ConfigFile = "webpack.config.js", HotModuleReplacement = true });
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc();
 
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });
            app.UseHangfireDashboard();
            app.UseHangfireServer();         
        }

        private void HangfireJobs(IServiceCollection services) {
           
            //Automatic job to delete unpaid orders 2h or older & it runs every min

            var sp = services.BuildServiceProvider();
            var orderRepository = sp.GetService<IOrderRepository>();
            RecurringJob.AddOrUpdate("Delete unpaid orders older than 2h", () => orderRepository.DeleteUnpaidOrders(), Cron.Minutely);
        }


    }
}
