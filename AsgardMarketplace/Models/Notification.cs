﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AsgardMarketplace.Models
{
    public class Notification : BaseModel
    {
        public string Message { get; set; }

        public Guid UserId { get; set; }
    }
}
