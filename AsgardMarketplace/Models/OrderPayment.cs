﻿using System;

namespace AsgardMarketplace.Models
{
    public class OrderPayment : BaseModel
    {
        public decimal TotalOrderSum { get; set; }

        public decimal Taxes { get; set; }

        public decimal TotalOrderSumAfterTax { get; set; }

        public Guid SellerId { get; set; }

        public Guid BuyerId { get; set; }

        public Guid OrderId { get; set; }

        public bool IsCompleted { get; set; }
    }
}
