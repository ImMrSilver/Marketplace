﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AsgardMarketplace.Models
{
    public class Item : BaseModel
    {
        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public decimal? Price { get; set; }

        public TaxRule TaxRule { get; set; }

        public byte[] ItemAsByteStream { get; set; }

        public ItemStatus Status { get; set; }

        [Required]
        public Guid OwnerId { get; set; }
    }
}
