﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AsgardMarketplace.Models
{
    public class Order : BaseModel
    {
        [Required]
        public Guid ItemId { get; set; }

        [Required]
        public decimal Price { get; set; }

        public OrderStatus Status { get; set; }

        [Required]
        public Guid SellerId { get; set; }
        [Required]
        public Guid BuyerId { get; set; }
    }
}
