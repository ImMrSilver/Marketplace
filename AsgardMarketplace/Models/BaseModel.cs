﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AsgardMarketplace.Models
{
    public class BaseModel
    {
        public Guid Id { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
