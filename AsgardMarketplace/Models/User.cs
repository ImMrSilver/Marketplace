﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AsgardMarketplace.Models
{
    public class User : BaseModel
    {
        [Required]
        public string Name { get; set; }

        public string SureName { get; set; }

        public string About { get; set; }

        [Required]
        public string Email { get; set; }

        public string CreditCardNumber { get; set; }
        
        public decimal AccountBalance { get; set; }
    }
}
