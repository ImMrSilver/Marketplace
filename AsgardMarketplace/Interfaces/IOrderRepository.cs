﻿using AsgardMarketplace.Models;
using System;
using System.Collections.Generic;

namespace AsgardMarketplace
{
    public interface IOrderRepository
    {
        Order Get(Guid orderId);
        IList<Order> Get();
        Order Put(Order order);
        Order Post(Order order);
        bool Delete(Guid orderId);
        void DeleteUnpaidOrders();
    }
}