﻿using System;

namespace AsgardMarketplace.Services
{
    public interface INotificationService
    {
        void SendNotification(Guid userId, string msg);
    }
}