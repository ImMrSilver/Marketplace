﻿using AsgardMarketplace.Models;
using System;
using System.Collections.Generic;

namespace AsgardMarketplace
{
    public interface IUserRepository
    {
        User Get(Guid userId);
        IList<User> Get();
        User Put(User user);
        User Post(User user);
        bool Delete(Guid userId);
    }
}