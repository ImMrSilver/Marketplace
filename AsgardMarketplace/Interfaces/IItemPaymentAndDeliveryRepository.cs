﻿using AsgardMarketplace.Models;
using System;
using System.Collections.Generic;

namespace AsgardMarketplace
{
    public interface IItemPaymentAndDeliveryRepository
    {
        (User,User,OrderPayment,Item) ProcessItemPaymentAndDelivery(User seller, User buyer, OrderPayment orderPayment, Item item);
    }
}