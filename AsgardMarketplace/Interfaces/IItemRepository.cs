﻿using AsgardMarketplace.Models;
using System;
using System.Collections.Generic;

namespace AsgardMarketplace
{
    public interface IItemRepository
    {
        Item Get(Guid itemId);
        IList<Item> Get();
        Item Put(Item item);
        Item Post(Item item);
        bool Delete(Guid itemId);
    }
}