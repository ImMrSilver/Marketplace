﻿using AsgardMarketplace.Models;
using System;
using System.Collections.Generic;

namespace AsgardMarketplace
{
    public interface IOrderPaymentRepository
    {
        OrderPayment Get(Guid id);
        IList<OrderPayment> Get();
        OrderPayment CreateOrderPaymentAndUpdateOrderStatus(OrderPayment orderPayment, Order order);
    }
}