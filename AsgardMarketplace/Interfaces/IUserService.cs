﻿using AsgardMarketplace.Models;
using System;
using System.Collections.Generic;

namespace AsgardMarketplace.Services
{
    public interface IUserService
    {
        User Get(Guid userId);
        IList<User> Get();
        User Put(User user);
        User Post(User user);
        bool Delete(Guid userId);
    }
}