﻿using AsgardMarketplace.Models;
using System;
using System.Collections.Generic;

namespace AsgardMarketplace.Services
{
    public interface IOrderService
    {
        Order Get(Guid orderId);
        IList<Order> Get();
        Order Put(Order order);
        Order Post(Order order);
        bool Delete(Guid orderId);
    }
}