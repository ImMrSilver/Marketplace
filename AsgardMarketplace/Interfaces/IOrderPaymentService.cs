﻿using AsgardMarketplace.Models;
using System;
using System.Collections.Generic;

namespace AsgardMarketplace
{
    public interface IOrderPaymentService
    {
        OrderPayment Get(Guid id);
        IList<OrderPayment> Get();
        OrderPayment ProcessOrderPayment(OrderPayment orderPayment);
    }
}