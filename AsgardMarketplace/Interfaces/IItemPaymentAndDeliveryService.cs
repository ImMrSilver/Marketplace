﻿using AsgardMarketplace.Models;
using System;
using System.Collections.Generic;

namespace AsgardMarketplace
{
    public interface IItemPaymentAndDeliveryService
    {
        (User, User, OrderPayment, Item) ProcessItemPaymentAndDelivery(OrderPayment orderPayment);
    }
}