﻿using AsgardMarketplace.Models;
using System;
using System.Collections.Generic;

namespace AsgardMarketplace.Services
{
    public interface IItemService
    {
        Item Get(Guid itemId);
        IList<Item> Get();
        Item Put(Item item);
        Item Post(Item item);
        bool Delete(Guid itemId);
    }
}