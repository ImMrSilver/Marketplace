﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AsgardMarketplace.DataViewModels
{
    public class UserViewModel
    {
        public Guid Id { get; set; }
        
        [Required]
        public string Name { get; set; }

        public string SureName { get; set; }

        public string About { get; set; }

        [Required]
        public string Email { get; set; }

        public string CreditCardNumber { get; set; }

        public decimal AccountBalance { get; set; }
    }
}
