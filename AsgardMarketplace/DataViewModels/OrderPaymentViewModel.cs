﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AsgardMarketplace.Models
{
    public class OrderPaymentViewModel
    {
        [Required]
        public Guid SellerId { get; set; }
        [Required]
        public Guid BuyerId { get; set; }
        [Required]
        public Guid OrderId { get; set; }

        public bool IsCompleted { get; set; }
    }
}
