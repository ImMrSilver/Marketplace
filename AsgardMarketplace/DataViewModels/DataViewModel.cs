﻿using AsgardMarketplace.Models;

namespace AsgardMarketplace.DataViewModels
{
    public class DataViewModel<T>
    {
        public DataViewModel(T value, string error = null)
        {
            if (!string.IsNullOrEmpty(error))
            {
                Status = ResponceStatus.Error;
                Message = error;
            }
            else
            {
                Status = ResponceStatus.Ok;
                Data = value;
            }
        }

        public ResponceStatus Status { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }
    }
}
