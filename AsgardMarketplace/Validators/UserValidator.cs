﻿using System;
using AsgardMarketplace.Models;
using Microsoft.Extensions.Logging;

namespace AsgardMarketplace.Validators
{
    public class UserValidator : BaseValidator
    {
        private ILogger<BaseValidator> _logger;
        private IUserRepository _userRepository;

        public UserValidator(IItemRepository itemRepository,
          IOrderRepository orderRepository,
          IUserRepository userRepository,
          IOrderPaymentRepository orderPaymentRepository,
          ILogger<BaseValidator> logger): base(itemRepository, orderRepository, userRepository, orderPaymentRepository, logger)
        {
            _userRepository = userRepository;
            _logger = logger;
        }

        public string Validate(User user)
        {
            if (string.IsNullOrEmpty(user.Name))
            {
                var msg = "Name has to have a value";
                _logger.LogError(msg);
                return msg;
            }
            if (string.IsNullOrEmpty(user.Email))
            {
                var msg = "Email has to have a value";
                _logger.LogError(msg);
                return msg;
            }
            return null;
        }

       
    }
}
