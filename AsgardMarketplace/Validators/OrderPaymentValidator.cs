﻿using System;
using AsgardMarketplace.Models;
using Microsoft.Extensions.Logging;

namespace AsgardMarketplace.Validators
{
    public class OrderPaymentValidator : BaseValidator
    {
        private ILogger<BaseValidator> _logger;
        private IUserRepository _userRepository;

        public OrderPaymentValidator(IItemRepository itemRepository,
          IOrderRepository orderRepository,
          IUserRepository userRepository,
          IOrderPaymentRepository orderPaymentRepository,
          ILogger<BaseValidator> logger): base(itemRepository, orderRepository, userRepository, orderPaymentRepository, logger)
        {
            _userRepository = userRepository;
            _logger = logger;
        }

        public string Validate(OrderPayment orderPayment)
        {
            string errorMessage = null;
            var buyer = _userRepository.Get(orderPayment.BuyerId);
            var BuyerHasNotEnoughMoney = orderPayment.TotalOrderSum > buyer.AccountBalance;
            if (BuyerHasNotEnoughMoney)
            {
                errorMessage = "Buyer has not enough money";
                _logger.LogError(errorMessage);
                return errorMessage;
            }
            errorMessage = UserExists(orderPayment.SellerId);
            if (errorMessage != null)
            {
                return errorMessage;
            }
            errorMessage = UserExists(orderPayment.BuyerId);
            if (errorMessage != null)
            {
                return errorMessage;
            }
            errorMessage = OrderExists(orderPayment.OrderId);
            if (errorMessage != null)
            {
                return errorMessage;
            }

            return null;
        }

       
    }
}
