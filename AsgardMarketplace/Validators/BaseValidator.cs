﻿using System;
using Microsoft.Extensions.Logging;

namespace AsgardMarketplace.Validators
{
    public class BaseValidator
    {
        private ILogger<BaseValidator> _logger;
        private IItemRepository _itemRepository;
        private IOrderRepository _orderRepository;
        private IOrderPaymentRepository _orderPaymentRepository;
        private IUserRepository _userRepository;

        public BaseValidator(
          IItemRepository itemRepository,
          IOrderRepository orderRepository,
          IUserRepository userRepository,
          IOrderPaymentRepository orderPaymentRepository,
          ILogger<BaseValidator> logger)
        {
            _itemRepository = itemRepository;
            _orderRepository = orderRepository;
            _userRepository = userRepository;
            _orderPaymentRepository = orderPaymentRepository;
            _logger = logger;
        }

        public string UserExists(Guid id)
        {
            var user = _userRepository.Get(id);
            if (user == null)
            {
                var msg = "User with this id:" + id.ToString() + " does not exist";
                _logger.LogError(msg);
                return msg;
            }
            return null;
        }

        public string ItemExists(Guid id)
        {
            var item = _itemRepository.Get(id);
            if (item == null)
            {
                var msg = "Item with this id:" + id.ToString() + " does not exist";
                _logger.LogError(msg);
                return msg;
            }
            return null;
        }

        public string OrderExists(Guid id)
        {
            var order = _orderRepository.Get(id);
            if (order == null)
            {
                var msg = "Order with this id:" + id.ToString() + " does not exist";
                _logger.LogError(msg);
                return msg;
            }
            return null;
        }

        public string OrderPaymnetExists(Guid id)
        {
            var orderPayment = _orderPaymentRepository.Get(id);
            if (orderPayment == null)
            {
                var msg = "OrderPaymnet with this id:" + id.ToString() + " does not exist";
                _logger.LogError(msg);
                return msg;
            }
            return null;
        }
    }
}
