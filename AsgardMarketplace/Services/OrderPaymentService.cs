﻿using System;
using System.Collections.Generic;
using AsgardMarketplace.Models;

namespace AsgardMarketplace.Services
{
    public class OrderPaymentService : IOrderPaymentService
    {
        private IOrderPaymentRepository _orderPaymentRepository;
        private IItemRepository _itemRepository;
        private IOrderRepository _orderRepository;
        private IUserRepository _userRepository;
        private IItemPaymentAndDeliveryService _itemPaymentAndDeliveryService;

        public OrderPaymentService(
            IOrderPaymentRepository orderPaymentRepository,
            IItemRepository itemRepository,
            IOrderRepository orderRepository,
            IUserRepository userRepository,
            IItemPaymentAndDeliveryService itemPaymentAndDeliveryService
            )
        {
            _orderPaymentRepository = orderPaymentRepository;
            _itemRepository = itemRepository;
            _orderRepository = orderRepository;
            _userRepository = userRepository;
            _itemPaymentAndDeliveryService = itemPaymentAndDeliveryService;
        }

        public IList<OrderPayment> Get()
        {
            return _orderPaymentRepository.Get();
        }

        public OrderPayment Get(Guid id)
        {
            var orderPayments = _orderPaymentRepository.Get(id);
            return orderPayments;
        }

        public OrderPayment ProcessOrderPayment(OrderPayment orderPayment)
        {
            var buyer = _userRepository.Get((Guid)orderPayment.BuyerId);

            orderPayment.CreationDate = DateTime.Now;
            var order = _orderRepository.Get(orderPayment.OrderId);
            var item = _itemRepository.Get(order.ItemId);

            CalculateTaxes(orderPayment, item);

            var createdOrderPayment = _orderPaymentRepository.CreateOrderPaymentAndUpdateOrderStatus(orderPayment, order);
            _itemPaymentAndDeliveryService.ProcessItemPaymentAndDelivery(createdOrderPayment);

            return createdOrderPayment;
        }

        private void CalculateTaxes(OrderPayment orderPayment, Item item)
        {
            orderPayment.IsCompleted = false;
            orderPayment.TotalOrderSum = (decimal)item.Price;
            switch (item.TaxRule)
            {
                case (TaxRule.Percent_5):
                    orderPayment.Taxes = (decimal)item.Price * 0.05m;
                    break;
                case (TaxRule.Percent_10):
                    orderPayment.Taxes = (decimal)item.Price * 0.1m;
                    break;
                case (TaxRule.Percent_25):
                    orderPayment.Taxes = (decimal)item.Price * 0.25m;
                    break;
                default: // TaxRule.Percent_10
                    orderPayment.Taxes = (decimal)item.Price * 0.1m;
                    break;
            }
            
            orderPayment.TotalOrderSumAfterTax = Math.Floor((orderPayment.TotalOrderSum - orderPayment.Taxes) * 100)/ 100 ;
        }
    }
}
