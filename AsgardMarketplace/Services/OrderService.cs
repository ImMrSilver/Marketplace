﻿using System;
using System.Collections.Generic;
using AsgardMarketplace.Models;

namespace AsgardMarketplace.Services
{
    public class OrderService : IOrderService
    {
        private IOrderRepository _orderRepository;

        public OrderService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public Order Get(Guid orderId)
        {
            return _orderRepository.Get(orderId);
        }

        public IList<Order> Get()
        {
            var orders = _orderRepository.Get();
            return orders;
        }

        public Order Post(Order order)
        {
            order.CreationDate = DateTime.Now;
            var createdOrder = _orderRepository.Post(order);
            return createdOrder;
        }

        public Order Put(Order order)
        {
            var updatedOrder = _orderRepository.Put(order);
            return updatedOrder;
        }

        public bool Delete(Guid orderId)
        {
            var IsDeleted = _orderRepository.Delete(orderId);
            return IsDeleted;
        }
    }
}
