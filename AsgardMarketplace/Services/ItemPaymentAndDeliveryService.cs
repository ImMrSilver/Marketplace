﻿using AsgardMarketplace.Models;
using System;

namespace AsgardMarketplace.Services
{
    public class ItemPaymentAndDeliveryService : IItemPaymentAndDeliveryService
    {
        private IOrderPaymentRepository _orderPaymentRepository;
        private IItemRepository _itemRepository;
        private IOrderRepository _orderRepository;
        private IUserRepository _userRepository;
        private INotificationService _notificationService;
        private IItemPaymentAndDeliveryRepository _itemPaymentAndDeliveryRepository;

        public ItemPaymentAndDeliveryService(
            IOrderPaymentRepository orderPaymentRepository,
            IItemRepository itemRepository,
            IOrderRepository orderRepository,
            IUserRepository userRepository,
            INotificationService notificationService,
            IItemPaymentAndDeliveryRepository itemPaymentAndDeliveryRepository)
        {
            _orderPaymentRepository = orderPaymentRepository;
            _itemRepository = itemRepository;
            _orderRepository = orderRepository;
            _userRepository = userRepository;
            _notificationService = notificationService;
            _itemPaymentAndDeliveryRepository = itemPaymentAndDeliveryRepository;
        }

        public (User, User, OrderPayment, Item) ProcessItemPaymentAndDelivery(OrderPayment orderPayment)
        {
            var order = _orderRepository.Get(orderPayment.OrderId);
            var seller = _userRepository.Get((Guid)order.SellerId);
            var buyer = _userRepository.Get((Guid)order.BuyerId);
            var item = _itemRepository.Get(order.ItemId);
            orderPayment.IsCompleted = true;

            TransferPayment(seller, buyer, orderPayment);
            DeliverItems(buyer, item);

            var result =  _itemPaymentAndDeliveryRepository.ProcessItemPaymentAndDelivery(seller, buyer, orderPayment, item);
            order.Status = OrderStatus.Closed;
            _orderRepository.Put(order);         

            _notificationService.SendNotification(seller.Id,"Item: "+ item.Name +" bought");
            _notificationService.SendNotification(buyer.Id, "Item: " + item.Name + " sold");

            return (result.Item1, result.Item2, result.Item3, result.Item4);
        }

        private void DeliverItems(User buyer, Item item)
        {
            item.OwnerId = buyer.Id;
            item.Status = ItemStatus.Inventory;
            item.Price = null;
        }

        private void TransferPayment(User seller, User buyer, OrderPayment orderPayment)
        {
            seller.AccountBalance = seller.AccountBalance + orderPayment.TotalOrderSumAfterTax;
            buyer.AccountBalance = buyer.AccountBalance - orderPayment.TotalOrderSum;
        }

    }
}
