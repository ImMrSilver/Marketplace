﻿using System;
using System.Collections.Generic;
using AsgardMarketplace.Models;

namespace AsgardMarketplace.Services
{
    public class UserService : IUserService
    {
        private IUserRepository _userRepository;

        public UserService(IUserRepository UserRepository)
        {
            _userRepository = UserRepository;
        }

        public User Get(Guid userId)
        {
            return _userRepository.Get(userId);
        }

        public IList<User> Get()
        {
            var Users = _userRepository.Get();
            return Users;
        }

        public User Post(User User)
        {
            User.CreationDate = DateTime.Now;
            var updatedUser = _userRepository.Post(User);
            return updatedUser;
        }

        public User Put(User User)
        {
            var createdUser = _userRepository.Put(User);
            return createdUser;
        }

        public bool Delete(Guid UserId)
        {
            var IsDeleted = _userRepository.Delete(UserId);
            return IsDeleted;
        }
    }
}
