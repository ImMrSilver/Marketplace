﻿using System;
using NLog;

namespace AsgardMarketplace.Services
{
    public class NotificationService:INotificationService
    {

        private readonly Logger _logger;
        private readonly IItemService _itemService;

        public NotificationService(IItemService itemService)
        {
                _itemService = itemService;
               _logger = LogManager.GetLogger("Notification");
        }

        public void SendNotification(Guid userId, string msg)
        {
            _logger.Log( LogLevel.Info ,userId +" "+ msg);
        }

    }
}
