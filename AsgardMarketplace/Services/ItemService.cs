﻿using AsgardMarketplace.Models;
using System;
using System.Collections.Generic;

namespace AsgardMarketplace.Services
{
    public class ItemService : IItemService
    {
        private IItemRepository _itemRepository;

        public ItemService(IItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public Item Get(Guid itemId)
        {
            return _itemRepository.Get(itemId);
        }

        public IList<Item> Get()
        {
            var items = _itemRepository.Get();
            return items;
        }

        public Item Post(Item item)
        {
            item.CreationDate = DateTime.Now;
            var createdItem = _itemRepository.Post(item);
            return createdItem;
        }

        public Item Put(Item item)
        {
            var updatedItem = _itemRepository.Put(item);
            return updatedItem;
        }

        public bool Delete(Guid itemId)
        {
            var IsDeleted = _itemRepository.Delete(itemId);
            return IsDeleted;
        }
    }
}
