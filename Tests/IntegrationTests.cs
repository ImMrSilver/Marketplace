using Microsoft.AspNetCore;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using AsgardMarketplace.Models;
using AsgardMarketplace.Services;
using AsgardMarketplace;
using AsgardMarketplace.Validators;
using AsgardMarketplace.Repository;
using Microsoft.EntityFrameworkCore;

namespace Tests
{
    [TestClass]
    public class IntegrationTests
    {
        private readonly IConfiguration _configuration;

        public IntegrationTests()
        {
            var settings = new List<KeyValuePair<string, string>> {};
            var builder = new ConfigurationBuilder().AddInMemoryCollection(settings);
            this._configuration = builder.Build();
        }

        /*
        1. Create 2 users, one seller one buyer         
        2. Create an item for sale
        3. Create an order
        4. Deliver payment
        5. Check if the buyer has received the item and seller has received the money
         */
        [TestMethod]
        public void WhenBuyingAnItemBuyerShouldGetTheItemAndSellerShouldGetTheMoney()
        {
            IServiceCollection services = InjectServices();
            var sp = services.BuildServiceProvider();
            var userService = sp.GetService<IUserService>();
            var itemService = sp.GetService<IItemService>();
            var orderService = sp.GetService<IOrderService>();
            var orderPaymentService = sp.GetService<IOrderPaymentService>();

            var Seller = new User()
            {
                Name = "Tom",
                Email = "tom@tom.com",
                AccountBalance = 1
            };
            var Buyer = new User()
            {
                Name = "Luke",
                Email = "Luke@tom.com",
                AccountBalance = 50
            };

            userService.Post(Seller);
            userService.Post(Buyer);
            var buyerId = Buyer.Id;
            var sellerId = Seller.Id;

            var Item1 = new Item()
            {
                Name = "Bane",
                Description = "2 handed axe",
                Price = 10,
                Status = ItemStatus.ForSale,
                OwnerId = Seller.Id

            };
            var Item2 = new Item()
            {
                Name = "Bow of Sundering",
                Description = "Powerfull bow",
                Status = ItemStatus.Inventory,
                OwnerId = Seller.Id
            };

            itemService.Post(Item1);
            itemService.Post(Item2);

            var order = new Order()
            {
                Price = (decimal)Item1.Price,
                BuyerId = Buyer.Id,
                SellerId = Seller.Id,
                ItemId = Item1.Id,
                Status = OrderStatus.Created
            };
            orderService.Post(order);

            var orderPayment = new OrderPayment()
            {
                OrderId = order.Id,
                BuyerId = Buyer.Id,
                SellerId = Seller.Id
            };

            var orderPayment2 = new OrderPayment()
            {
                OrderId = order.Id,
                BuyerId = Buyer.Id,
                SellerId = Seller.Id
            };

            orderPaymentService.ProcessOrderPayment(orderPayment);

            var deliveredItem = itemService.Get(Item1.Id);
            var updatedSellerUser = userService.Get(sellerId);
            var updatedBuyerUser = userService.Get(buyerId);
            var udatedOrder = orderService.Get(order.Id);
            var udatedOrderPayment = orderPaymentService.Get(orderPayment.Id);

            //Assert
            Assert.AreEqual(buyerId, deliveredItem.OwnerId);
            Assert.AreEqual(updatedSellerUser.AccountBalance, 10.5m);
            Assert.AreEqual(updatedBuyerUser.AccountBalance, 40m);
            Assert.AreEqual(udatedOrder.Status, OrderStatus.Closed);
            Assert.AreEqual(udatedOrderPayment.IsCompleted, true);
            
        }

        private static IServiceCollection InjectServices()
        {
            IServiceCollection services = new ServiceCollection();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserRepository, UserRepository>();

            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IOrderRepository, OrderRepository>();

            services.AddScoped<IItemService, ItemService>();
            services.AddScoped<IItemRepository, ItemRepository>();

            services.AddScoped<IOrderPaymentService, OrderPaymentService>();
            services.AddScoped<IOrderPaymentRepository, OrderPaymentRepository>();

            services.AddScoped<INotificationService, NotificationService>();

            services.AddScoped<IItemPaymentAndDeliveryService, ItemPaymentAndDeliveryService>();
            services.AddScoped<IItemPaymentAndDeliveryRepository, ItemPaymentAndDeliveryRepository>();

            services.AddScoped<BaseValidator>();
            services.AddScoped<UserValidator>();

            services.AddDbContext<AsgardMarketplaceContext>(opt =>
            {
                opt.UseInMemoryDatabase("AsgardMarketplace");
                opt.ConfigureWarnings(x => x.Ignore(Microsoft.EntityFrameworkCore.Diagnostics.InMemoryEventId.TransactionIgnoredWarning));
            });
            return services;
        }
    }
}
